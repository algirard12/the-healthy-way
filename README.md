# The Healthy Way

## Description
This tiny project is a way to learn some basics in web developement (Html, CSS and Javascript).
That is a showcase site for a fictional vegan restaurant.

## Visuals
You can see the mock-ups done on Figma here by clicking on this link : [The Healthy Way](https://www.figma.com/file/UvjsuzTvGqDIudq7Fg3pQT/The-Healthy-Way?type=design&node-id=0%3A1&mode=design&t=APrirfeZBYBuSmkY-1)


## Project status
The project is currently on going.
